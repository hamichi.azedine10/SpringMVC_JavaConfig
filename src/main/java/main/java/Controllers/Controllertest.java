package main.java.Controllers;

import main.java.Service.Iservice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/testcontroller")
public class Controllertest {

    Iservice iservice;

    public Iservice getIservice() {return iservice;}
    @Autowired
    public void setIservice(Iservice iservice) {this.iservice = iservice;}

    @ResponseBody
    @GetMapping("/testindex")
    public  String index(){
        return "Ceci est un test";
    }

    @GetMapping("/hellopage")
    public String hellopage(){
        return  "hellojsp";
    }

    @ResponseBody
    @GetMapping("/serviceprint")
    public  String afficheservice(){
       return  iservice.getservice();
    }

    @ResponseBody
    @GetMapping ("/servicedata")
    public String afficherdata ()
    {
        System.out.println("afficherdata called ..." );
        return iservice.getserviceformdao();
    }
}
