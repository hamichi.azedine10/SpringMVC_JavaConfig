package main.java.configuration;

import org.springframework.web.WebApplicationInitializer;

import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.ServletContext;
import javax.servlet.ServletRegistration;

public class webApplicationConfig_onstartup /*implements WebApplicationInitializer*/ {

    /**
     * Configure the given {@link ServletContext} with any servlets, filters, listeners
     * context-params and attributes necessary for initializing this web application. See
     * examples {@linkplain WebApplicationInitializer above}.
     *
     * @param servletContext the {@code ServletContext} to initialize
     */
   // @Override
    public void onStartup(ServletContext servletContext) {

        // Load Spring web application configuration for root context
        AnnotationConfigWebApplicationContext rootApplicationContext = new AnnotationConfigWebApplicationContext();
        rootApplicationContext.register(RootApplicationContext.class);
        //register the DispatcherServlet with the root in  contextservlet
        servletContext.addListener(new ContextLoaderListener(rootApplicationContext));

        // Load Spring web application configuration for servlet despatchers
        AnnotationConfigWebApplicationContext applicationContext = new AnnotationConfigWebApplicationContext();
        applicationContext.register(WebConfig.class);

        // Create the DispatcherServlets
        DispatcherServlet Myservlet = new DispatcherServlet(applicationContext);
        //register the DispatcherServlet with the servlet context
        ServletRegistration.Dynamic registration = servletContext.addServlet("Mydispatcher", Myservlet);
        registration.setLoadOnStartup(1);
        registration.addMapping("/Onstartup/*");
    }
}
