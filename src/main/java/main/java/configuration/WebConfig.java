package main.java.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

@Configuration
@EnableWebMvc
@PropertySource("classpath:ConfigContext.properties")
@ComponentScan(basePackages = {"main.java.Controllers"})
public class WebConfig {
    @Value("${context.interviewer.prefix}")String prefixe;
    @Value("${context.interviewer.suffix}") String suffix;
    // create beans
    @Bean
   public  InternalResourceViewResolver internalResourceViewResolver (){

       InternalResourceViewResolver internalResourceViewResolver =   new InternalResourceViewResolver();

       internalResourceViewResolver.setPrefix(prefixe);
       internalResourceViewResolver.setSuffix(suffix);
       return internalResourceViewResolver;
   }

}
