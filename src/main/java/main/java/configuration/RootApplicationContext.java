package main.java.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
@EnableWebMvc
//@PropertySource("classpath:ConfigContext.properties")
@ComponentScan(basePackages = {"main.java.Service","main.java.Daos"})
public class RootApplicationContext {
}
