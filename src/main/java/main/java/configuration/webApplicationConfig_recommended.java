package main.java.configuration;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

public class webApplicationConfig_recommended extends AbstractAnnotationConfigDispatcherServletInitializer {
    /**
     * Specify {@code @Configuration} and/or {@code @Component} classes for the
     * {@linkplain #createRootApplicationContext() root application context}.
     *
     * @return the configuration for the root application context, or {@code null}
     * if creation and registration of a root context is not desired
     */
    @Override
    protected Class<?>[] getRootConfigClasses() {
        Class [] arr = {RootApplicationContext.class};
        return arr;
    }

    /**
     * Specify {@code @Configuration} and/or {@code @Component} classes for the
     * {@linkplain #createServletApplicationContext() Servlet application context}.
     *
     * @return the configuration for the Servlet application context, or
     * {@code null} if all configuration is specified through root config classes.
     */
    @Override
    protected Class<?>[] getServletConfigClasses() {
        Class arr[] = {WebConfig.class};
        return arr;
    }

    /**
     * Specify the servlet mapping(s) for the {@code DispatcherServlet} &mdash;
     * for example {@code "/"}, {@code "/app"}, etc.
     *
     * //@see #registerDispatcherServlet(ServletContext)
     */
    @Override
    protected String[] getServletMappings() {
        String[] arr = {"/RecommendedConfig/*"};
        return  arr;
    }

    /**
     *  rename dispatcher
     * @return
     */
    @Override
    protected String getServletName() {
        return "myservletdispatcher";
    }
}
