package main.java.Service;

import main.java.Daos.IDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

@Service
public class ServiceImpl implements Iservice {

    IDao idao;

    public IDao getIdao() {return idao;}

    @Autowired
    public void setIdao(@Qualifier("daoImpl") IDao idao) {
        this.idao = idao;
    }

    @Override
    public String getservice() {
        return "donnée de la couche service ";
    }

    @Override
    public String getserviceformdao() {
     return idao.getdao();
    }
}
